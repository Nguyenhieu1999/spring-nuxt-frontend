// import enquireJs from 'enquire.js'

export const DEVICE_TYPE = {
  DESKTOP: 'desktop',
  TABLET: 'tablet',
  MOBILE: 'mobile'
}

export const deviceEnquire = function (callback) {
  const matchDesktop = {
    match: () => {
      callback && callback(DEVICE_TYPE.DESKTOP)
    }
  }

  const matchLablet = {
    match: () => {
      callback && callback(DEVICE_TYPE.TABLET)
    }
  }

  const matchMobile = {
    match: () => {
      callback && callback(DEVICE_TYPE.MOBILE)
    }
  }

  // screen and (max-width: 1087.99px)
  // window && (enquireJs
  //   .register('screen and (max-width: 576px)', matchMobile)
  //   .register('screen and (min-width: 576px) and (max-width: 1199px)', matchLablet)
  //   .register('screen and (min-width: 1200px)', matchDesktop))
  var mq = window.matchMedia("(max-width: 576px)")
  if (mq.matches) {
    matchMobile.match()
  }
  var mq2 = window.matchMedia("(screen and (min-width: 576px) and (max-width: 1199px)")
  if (mq2.matches) {
    matchLablet.match()
  }
  var mq3 = window.matchMedia("(min-width: 1200px)")
  if (mq3.matches) {
    matchDesktop.match()
  }
}
