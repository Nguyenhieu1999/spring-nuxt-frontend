import Cookie from 'js-cookie'

export const setToken = (token) => {
  Cookie.set('token', token)
}

export const unsetToken = () => {
  Cookie.remove('token')
}

export const getTokenInServer = (req) => {
  if (!req.headers.cookie) return
  const tokenCookie = req.headers.cookie.split(';').find(c => c.trim().startsWith('token='))
  if (!tokenCookie) return
  const token = tokenCookie.split('=')[1]
  return token
}

export const getTokenInClient = () => {
  return Cookie.get('token') || ''
}
