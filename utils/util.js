import notification from 'ant-design-vue/lib/notification'

export function triggerWindowResizeEvent() {
  const event = document.createEvent('HTMLEvents')
  event.initEvent('resize', true, true)
  event.eventType = 'message'
  window.dispatchEvent(event)
}

export const handleSystemState = (client, store, error_code, error) => {

  if (error_code == 20001) {
    store.dispatch('FedLogOut')
    client && notification.warn({
      message: 'system information',
      description: 'Not logged in, please log in'
    });
  }

  if (error_code == 30001) {
    error({ statusCode: 500 })
  }

  if (error_code == 70003) {
    store.dispatch('FedLogOut')
    client && notification.warn({
      message: 'system information',
      description: 'Login timed out, please log in again'
    });
  }

  if (error_code == 70001) {
    error({ statusCode: 403 })
  }
}
