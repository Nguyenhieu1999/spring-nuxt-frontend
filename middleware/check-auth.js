import { getTokenInServer, getTokenInClient } from '@/utils/token'

export default function (context) {
  const { store, error, route } = context
  const token = process.server ? getTokenInServer(context.req) : getTokenInClient()
  if (token) {
    if (!store.getters.token) {
      store.commit('SET_TOKEN', token)
    }
    if (!store.getters.nickname) {
      return store.dispatch('GetInfo')
    }

  } else {
    const routePath = route.path
    const extraPath = ['/write']

    if (extraPath.some(p => routePath.startsWith(p))) {
      error({ statusCode: 403 })
    }
  }
}
