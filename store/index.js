import Vue from 'vue'
import Vuex from 'vuex'

import app from './modules/app'
import user from './modules/user'
// import user from './modules/user'
// import permission from './modules/permission'
import getters from './getters'

Vue.use(Vuex)

const store = () => new Vuex.Store({
  modules: {
    app,
    user
    // permission
  },
  state: {},
  mutations: {},
  actions: {
    nuxtServerInit({commit}, {req}) {
      commit('SET_ERROR_CODE', '')
      commit('SET_REFRESH_TOKEN', '')
    }
  },
  getters
})
export default store
