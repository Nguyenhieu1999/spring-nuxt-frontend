import { ACCESS_TOKEN } from '@/store/mutation-types'
import { setToken, unsetToken } from '@/utils/token'

const user = {
  state: {
    token: '',
    refresh_token: '',
    name: '',
    welcome: '',
    avatar: '',
    error_code: ''
  },

  mutations: {
    SET_TOKEN: (state, token) => {
      state.token = token
    },
    SET_REFRESH_TOKEN: (state, refresh_token) => {
      state.refresh_token = refresh_token
    },
    SET_NAME: (state, { name, welcome }) => {
      state.name = name
      state.welcome = welcome
    },
    SET_AVATAR: (state, avatar) => {
      state.avatar = avatar
    },
    SET_ERROR_CODE: (state, error_code) => {
      state.error_code = error_code
    }
  },

  actions: {
    Login({ commit }, userInfo) {
      return new Promise((resolve, reject) => {
        this.$axios.$post('/auth/login', userInfo).then(res => {

          const { data = {} } = res
          setToken(data[ACCESS_TOKEN])
          commit('SET_TOKEN', data[ACCESS_TOKEN])

          resolve(res)
        }).catch(error => {
          reject(error)
        })
      })
    },
    GetInfo({ commit }) {
      return new Promise((resolve, reject) => {
        this.$axios.$get('/users/currentUser').then(res => {
          const { data = {} } = res

          commit('SET_NAME', { name: data.nickname, welcome: 'Welcome' })
          commit('SET_AVATAR', data.avatar)

          resolve(res)
        }).catch(error => {
          reject(error)
        })
      })
    },

    Logout({ commit, state }) {
      return new Promise((resolve) => {

        commit('SET_TOKEN', '')
        commit('SET_NAME', '')
        commit('SET_AVATAR', '')
        unsetToken()

        this.$axios.$get('/auth/logout').then(() => {
          resolve()
        }).catch(() => {
          resolve()
        })
      })
    },
    FedLogOut({ commit, state }) {
      return new Promise((resolve) => {

        commit('SET_TOKEN', '')
        commit('SET_NAME', '')
        commit('SET_AVATAR', '')
        unsetToken()

        resolve()
      })
    }

  }
}

export default user
