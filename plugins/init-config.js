
import config from '@/config/defaultSettings'

export default function (context) {
  const store = context.store
  store.commit('SET_SIDEBAR_TYPE', true)
  store.commit('TOGGLE_THEME', config.navTheme)
  store.commit('TOGGLE_LAYOUT_MODE', config.layout)
  store.commit('TOGGLE_FIXED_HEADER', config.fixedHeader)
  store.commit('TOGGLE_FIXED_SIDERBAR', config.fixSiderbar)
  store.commit('TOGGLE_CONTENT_WIDTH', config.contentWidth)
  store.commit('TOGGLE_FIXED_HEADER_HIDDEN', config.autoHideHeader)
  store.commit('TOGGLE_WEAK', config.colorWeak)
  store.commit('TOGGLE_COLOR', config.primaryColor)
}
