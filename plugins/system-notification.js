
import { handleSystemState } from '@/utils/util'
import { setToken } from '@/utils/token'

export default function (context) {
  const { store, error } = context

  if (store.getters.error_code) {
    handleSystemState(true, store, store.getters.error_code, error)
    store.commit('SET_ERROR_CODE', '')
  }

  if (store.getters.refresh_token) {
    setToken(store.getters.refresh_token)
    store.commit('SET_REFRESH_TOKEN', '')
  }

}
