
import Vue from 'vue'

Vue.prototype.auth = function (type) {
  switch (type) {
    case 'user':
      return !!this.$store.getters.token
  }
  return false
}





