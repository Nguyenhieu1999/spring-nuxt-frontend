import { ACCESS_TOKEN } from '@/store/mutation-types'
import { handleSystemState } from '@/utils/util'
import { setToken } from '@/utils/token'

export default function (context) {
  const { $axios, store, error } = context

  $axios.onRequest(config => {
    console.log('Making request to ' + config.url)

    if (store.getters.token) {
      config.headers[ACCESS_TOKEN] = store.getters.token
    }
  })

  $axios.onResponse(response => {
    if (response.headers['access-token']) {
      store.commit('SET_TOKEN', response.headers['access-token'])

      if (process.server) {
        store.commit('SET_REFRESH_TOKEN', response.headers['access-token'])
      } else {
        setToken(response.headers['access-token'])
      }
    }

    const data = response.data;

    if (process.server) {
      if (!store.getters.error_code) {
        store.commit('SET_ERROR_CODE', data.error_code)
      }
    }

    handleSystemState(!process.server, store, data.error_code, error)

  })

  $axios.onError(err => {
    error({ statusCode: 500 })
  })
}
