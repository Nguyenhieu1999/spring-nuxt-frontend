export const menus = [
  {
    path: '/',
    name: 'index',
    meta: { title: 'Home', icon: 'home', permission: ['dashboard'] }
  },
  {
    path: '/categories',
    name: 'categories',
    meta: { title: 'Category', icon: 'pushpin', permission: ['dashboard'] }
  },
  {
    path: '/tags',
    name: 'tags',
    meta: { title: 'Tags', icon: 'tags', permission: ['dashboard'] }
  },
  {
    path: '/archives',
    name: 'archives',
    meta: { title: 'Database', icon: 'database', permission: ['dashboard'] }
  },
  {
    path: '/about',
    name: 'about',
    meta: { title: 'About', icon: 'idcard', permission: ['dashboard'] }
  }
]
