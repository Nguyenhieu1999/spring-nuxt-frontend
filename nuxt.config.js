const pkg = require('./package')


module.exports = {
  mode: 'universal',
  router: {
    middleware: 'check-auth'
  },

  head: {
    title: 'Hieu Nguyen',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'keywords', name: 'keywords', content: '' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  loading: { color: '#1890ff' },

  css: [
    '@/assets/css/app.less',
    'ant-design-vue/dist/antd.css',
    'mavon-editor/dist/css/index.css'

  ],

  plugins: [
    '@/plugins/axios',
    '@/plugins/antd-ui',
    '@/plugins/init-config',
    { src: '@/plugins/auto-resize', ssr: false },
    { src: '@/plugins/mavon-editor', ssr: false },
    { src: '@/plugins/auth', ssr: true },
    { src: '@/plugins/system-notification', ssr: false }
  ],

  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios'
  ],

  axios: {

    baseURL: 'http://localhost:8888',
    debug: process.env.NODE_ENV !== 'production'
  },

  build: {

    extend(config, ctx) {

    }
  }
}
